interface IProps {
    name: string;
    placeholder?: string;
    label: string;
    type: string;
    messageError?: string;
    required: boolean;
    value: string | number;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<IProps> = ({ name, placeholder, label, type, messageError, required, value, onChange }) => {
    return (
        <>
            <label htmlFor={name} className="f6 b db mb2">{label} <span className="bold black">{required ? '*' : ''}</span></label>
            <input
                id={name}
                className="input-reset ba b--black-20 pa2 mb2 db w-100"
                name={name}
                type={type}
                placeholder={placeholder}
                aria-describedby={name}
                value={value}
                required={required}
                onChange={onChange}
                min="0" 
            />
            <small id={name} className="f6 black-60 db mb2 red pl1">{messageError}</small>
        </>
    );
}

export default Input;
