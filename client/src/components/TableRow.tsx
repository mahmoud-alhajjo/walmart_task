import { IState as Props } from '../App';

interface IProps {
    nutritionItem: Props['nutrition'][0];
    ids: number[];
    setIds: React.Dispatch<React.SetStateAction<number[]>>;
    selectItem: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const TableRow: React.FC<IProps> = ({ nutritionItem, ids, selectItem }) => {
    return (
        <tr>
            <td className="pv3 ph3 bb b--black-20 tc">
                <input
                    type="checkbox"
                    value={nutritionItem.id}
                    onChange={selectItem}
                    checked={ids.includes(nutritionItem.id)}
                />
            </td>
            <td className="pv3 pr3 bb b--black-20 tc">{nutritionItem.dessert}</td>
            <td className="pv3 pr3 bb b--black-20 tc">{nutritionItem.nutritionInfo.calories}</td>
            <td className="pv3 pr3 bb b--black-20 tc">{nutritionItem.nutritionInfo.fat}</td>
            <td className="pv3 pr3 bb b--black-20 tc">{nutritionItem.nutritionInfo.carb}</td>
            <td className="pv3 pr3 bb b--black-20 tc">{nutritionItem.nutritionInfo.protein}</td>
        </tr>
    );
}

export default TableRow;
