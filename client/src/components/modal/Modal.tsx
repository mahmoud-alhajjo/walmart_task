import './modal.css';

interface IProps {
    showModal: boolean;
    setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
    children: JSX.Element;
}

const Modal: React.FC<IProps> = ({ showModal, setShowModal, children }) => {
    const showHideClassName = showModal ? "db" : "dn";

    return (
        <div className={`fixed top-0 left-0 w-100 h-100 bg-black-60 ${showHideClassName}`}>
            <section className="fixed w-auto-ns w-80 h-auto center bg-white modal-main">
                {children}
                <button className="absolute top-0 right-0 w2 h2 bg-light-red modal-main-close br4 bw2 pointer tc" type="button" onClick={() => setShowModal(!showModal)}>
                    X
                </button>
            </section>
        </div>
    );
};

export default Modal;