import React, { useState } from 'react';
import { IState as Props } from '../App';
import Input from './Input';
import Validator from '../Helpers/Validator';
import { useMutation } from 'react-query';
import { gql } from 'graphql-request'
import { GraphQLResponse } from 'graphql-request/dist/types';
import graphqlRequestClient from '../lib/clients/graphqlRequestClient';
import { FaExclamationTriangle, FaCheck } from 'react-icons/fa';

interface IProps {
    nutrition: Props['nutrition'];
    setNutrition: React.Dispatch<React.SetStateAction<Props['nutrition']>>;
    showModal: boolean;
    setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const ADD_Nutrition = gql`
mutation CreateNutrition($nutrition: NutritionData) {
  createNutrition(nutrition: $nutrition) {
    error
    success
  }
}`;

const Form: React.FC<IProps> = ({ nutrition, setNutrition, showModal, setShowModal }) => {

    const { mutate, isLoading, error } = useMutation<GraphQLResponse, Error, {}>(
        'createNutrition',
        async (addNewItem) => {
            return graphqlRequestClient.request(ADD_Nutrition, addNewItem)
        },
        {
            onSuccess: (data) => { setNutrition([...nutrition, input] as Props['nutrition']) }
        }
    )

    const [input, setInput] = useState({
        id: Math.floor(Math.random() * (1000 - 3 + 1) + 3),
        dessert: "",
        nutritionInfo: {
            calories: "",
            fat: "",
            carb: "",
            protein: ""
        }
    })
    const [messageError, setMessageError] = useState({
        dessertError: "",
        caloriesError: "",
        fatError: "",
        carbError: "",
        proteinError: ""
    })

    const validationForm = (name: string, value: string): void => {
        let messagesError = { ...messageError }
        switch (name) {
            case "dessert":
                let dessertError = Validator.getNameError(value)
                messagesError = { ...messagesError, dessertError: dessertError };
                setMessageError(messagesError)
                break;
            case "calories":
                let caloriesError = Validator.getNumberError(value)
                messagesError = { ...messagesError, caloriesError: caloriesError };
                setMessageError(messagesError)
                break;
            case "fat":
                let fatError = Validator.getNumberError(value)
                messagesError = { ...messagesError, fatError: fatError };
                setMessageError(messagesError)
                break;
            case "carb":
                let carbError = Validator.getNumberError(value)
                messagesError = { ...messagesError, carbError: carbError };
                setMessageError(messagesError)
                break;
            case "protein":
                let proteinError = Validator.getNumberError(value)
                messagesError = { ...messagesError, proteinError: proteinError };
                setMessageError(messagesError)
                break;
            default:
                break;
        }
    }

    const handelChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        let data = { ...input };
        let name = event.target.name;
        let value = event.target.value;
        validationForm(name, value)
        if (name === 'dessert' || name === 'id') {
            data = { ...data, [name]: value };
        } else {
            data = {
                ...data,
                nutritionInfo: {
                    ...data.nutritionInfo,
                    [name]: value
                }
            };
        }
        setInput(data)
    }

    const _handelSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        const newItem = {
            id: input.id,
            dessert: input.dessert,
            nutritionInfo: {
                calories: parseInt(input.nutritionInfo.calories),
                fat: parseInt(input.nutritionInfo.fat),
                carb: parseInt(input.nutritionInfo.carb),
                protein: parseInt(input.nutritionInfo.protein)
            }
        }
        event.preventDefault();
        mutate({ nutrition: newItem })
        setInput({
            id: Math.floor(Math.random() * (1000 - 3 + 1) + 3),
            dessert: "",
            nutritionInfo: {
                calories: "",
                fat: "",
                carb: "",
                protein: ""
            }
        })
        setShowModal(!showModal)
    };

    if (isLoading) return <h2>Loading.....</h2>
    if (error) return <h2>error {error.message}</h2>

    return (
        <form className="ph4 pv2 black-70" onSubmit={_handelSubmit}>
            <h5 className="tracked tc mv3 pv2 ph3 bg-gold white"><FaExclamationTriangle /> Please fill all details before you submit</h5>
            <div>
                <Input
                    name='dessert'
                    placeholder='Name'
                    label='Dessert'
                    type='text'
                    messageError={(messageError.dessertError === "") ? '' : messageError.dessertError}
                    required={true}
                    value={input.dessert}
                    onChange={handelChange}
                />
                <Input
                    name='calories'
                    placeholder='Calories'
                    label='Calories'
                    type='number'
                    messageError={(messageError.caloriesError === "") ? '' : messageError.caloriesError}
                    required={true}
                    value={input.nutritionInfo.calories}
                    onChange={handelChange}
                />
                <Input
                    name='fat'
                    placeholder='Fat'
                    label='Fat'
                    type='number'
                    messageError={(messageError.fatError === "") ? '' : messageError.fatError}
                    required={true}
                    value={input.nutritionInfo.fat}
                    onChange={handelChange}
                />
                <Input
                    name='carb'
                    placeholder='Carb'
                    label='Carb'
                    type='number'
                    messageError={(messageError.carbError === "") ? '' : messageError.carbError}
                    required={true}
                    value={input.nutritionInfo.carb}
                    onChange={handelChange}
                />
                <Input
                    name='protein'
                    placeholder='Protein'
                    label='Protein'
                    type='number'
                    messageError={(messageError.proteinError === "") ? '' : messageError.proteinError}
                    required={true}
                    value={input.nutritionInfo.protein}
                    onChange={handelChange}
                />
            </div>
            <button type="submit" className="f6 w-100 no-underline br1 ph3 pv2 mv3 white bg-dark-green ttu bw0 pointer"><FaCheck />  Submit</button>
        </form>
    );
}

export default Form;
