import React, { useMemo, useState } from 'react';
import { IState as Props } from '../App';
import { useMutation } from 'react-query';
import { gql } from 'graphql-request'
import { GraphQLResponse } from 'graphql-request/dist/types';
import graphqlRequestClient from '../lib/clients/graphqlRequestClient'
import TableRow from './TableRow';
import { FaPlus, FaTrash, FaSortAlphaDown, FaSortNumericDown, FaSortAlphaDownAlt, FaSortNumericDownAlt } from 'react-icons/fa';
import { Sort } from '../Helpers/utlis'

const ADD_Nutrition = gql`
mutation DeleteNutrition($ids: [Int]) {
    deleteNutrition(ids: $ids) {
      success
      error
    }
}`;

interface IProps {
    nutrition: Props['nutrition'];
    setNutrition: React.Dispatch<React.SetStateAction<Props['nutrition']>>;
    ids: number[];
    setIds: React.Dispatch<React.SetStateAction<number[]>>;
    showModal: boolean;
    setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const TableNutrition: React.FC<IProps> = ({ nutrition, setNutrition, ids, setIds, showModal, setShowModal }) => {

    const { mutate, isLoading, error } = useMutation<GraphQLResponse, Error, {}>(
        'deleteNutrition',
        async (deleteItem) => {
            return graphqlRequestClient.request(ADD_Nutrition, deleteItem)
        },
        {
            onSuccess: (data) => {
                const remainingItems: Props['nutrition'] = nutrition.filter(
                    (item) => !ids.includes(item.id)
                );
                setNutrition(remainingItems);
            }
        }
    )
    const [sort, setSort] = useState('');
    const [typeOfDate, setTypeOfDate] = useState('');
    const [typeOfSort, setTypeOfSort] = useState(!true);
    const sortFncMemo = (sort: string, typeOfSort: boolean, typeOfDate: string) => {
        setTypeOfDate(typeOfDate)
        setSort(sort)
        setTypeOfSort(typeOfSort)
        setNutrition(sorted)
    }
    const sorted = useMemo(() => {
        if (!sort) return nutrition;
        let sortNutrition = Sort(nutrition, sort, typeOfSort, typeOfDate)
        return sortNutrition;
    }, [nutrition, sort, typeOfSort, typeOfDate]);


    const renderRows = (): JSX.Element[] => {
        return nutrition.map((nutritionItem, index) => {
            return <TableRow
                key={index + 1}
                nutritionItem={nutritionItem}
                ids={ids}
                setIds={setIds}
                selectItem={selectItem}
            />
        })
    }

    const selectItem = (event: React.ChangeEvent<HTMLInputElement>) => {
        const selectedId = parseInt(event.target.value);
        if (ids.includes(selectedId) && !event.target.name) {
            const newIds = ids.filter((id) => id !== selectedId);
            setIds(newIds);
        } else if (event.target.name === 'all' && event.target.checked) {
            setIds(nutrition.map(nutritionItem => nutritionItem.id))
        }
        else if (event.target.name === 'all' && !event.target.checked) {
            setIds([])
        }
        else {
            const newIds = [...ids];
            newIds.push(selectedId);
            setIds(newIds);
        }
    };

    const removeItems = () => mutate({ ids: ids });

    if (isLoading) return <h2>Loading.....</h2>
    if (error) return <h2>error {error.message}</h2>

    return (
        <>
            <div className="ph4 pv2">
                <div className="overflow-auto">
                    <div className='flex justify-between items-center w-100 ph3 bg-washed-red'>
                        <h6 className="ma0 f6 fw7-ns fw4 dib dark-red">selected</h6>
                        <div>
                            <button className="f7 fw7-ns fw4 br1 pv2 ph3 mv2 dib dark-green bg-white ttu ma1 hover-white hover-bg-green bw0 pointer" onClick={() => setShowModal(!showModal)}><FaPlus /> Add New</button>
                            <button className="f7 fw7-ns fw4 dim br1 pv2 ph3 mv2 dib white bg-light-red ttu ma1 bw0 pointer" onClick={removeItems}><FaTrash /> delete</button>
                        </div>
                    </div>
                    <table className="f6-ns f7 w-100 mw8 center pb2 aspect-ratio aspect-ratio--8x5 mb4" cellSpacing="0">
                        <thead>
                            <tr className="mt3 pv3">

                                <th className="bb b--black-20 tl pv3 ph3 bg-white tc">
                                    <input
                                        type="checkbox"
                                        name="all"
                                        onChange={selectItem}
                                        checked={ids.length > 0 && nutrition.length === ids.length}
                                    />
                                </th>
                                <th className="fw7-ns fw4 bb b--black-20 tl pb3 pr3 bg-white tc pv3 pointer" onClick={() => sortFncMemo('sortByName', !typeOfSort, "dessert")}>Dessert(100g serving){(typeOfSort && sort === "sortByName" && typeOfDate === "dessert") ? <FaSortAlphaDown size={14} color={(typeOfDate === "dessert") ? 'blue' : ''} /> : <FaSortAlphaDownAlt size={14} color={(typeOfDate === "dessert") ? 'red' : ''} />}</th>
                                <th className="fw7-ns fw4 bb b--black-20 tl pb3 pr3 bg-white tc pv3 pointer" onClick={() => sortFncMemo('sortByNumber', !typeOfSort, "calories")}>Calories {(typeOfSort && sort === "sortByNumber" && typeOfDate === "calories") ? <FaSortNumericDown size={14} color={(typeOfDate === "calories") ? 'blue' : ''} /> : <FaSortNumericDownAlt size={14} color={(typeOfDate === "calories") ? 'red' : ''} />}</th>
                                <th className="fw7-ns fw4 bb b--black-20 tl pb3 pr3 bg-white tc pv3 pointer" onClick={() => sortFncMemo('sortByNumber', !typeOfSort, "fat")}>Fat (g) {(typeOfSort && sort === "sortByNumber" && typeOfDate === "fat") ? <FaSortNumericDown size={14} color={(typeOfDate === "fat") ? 'blue' : ''} /> : <FaSortNumericDownAlt size={14} color={(typeOfDate === "fat") ? 'red' : ''} />}</th>
                                <th className="fw7-ns fw4 bb b--black-20 tl pb3 pr3 bg-white tc pv3 pointer" onClick={() => sortFncMemo('sortByNumber', !typeOfSort, "carb")}>Carb (g){(typeOfSort && sort === "sortByNumber" && typeOfDate === "carb") ? <FaSortNumericDown size={14} color={(typeOfDate === "carb") ? 'blue' : ''} /> : <FaSortNumericDownAlt size={14} color={(typeOfDate === "carb") ? 'red' : ''} />}</th>
                                <th className="fw7-ns fw4 bb b--black-20 tl pb3 pr3 bg-white tc pv3 pointer" onClick={() => sortFncMemo('sortByNumber', !typeOfSort, "protein")}>Protein (g) {(typeOfSort && sort === "sortByNumber" && typeOfDate === "protein") ? <FaSortNumericDown size={14} color={(typeOfDate === "protein") ? 'blue' : ''} /> : <FaSortNumericDownAlt size={14} color={(typeOfDate === "protein") ? 'red' : ''} />}</th>
                            </tr>
                        </thead>
                        <tbody className="lh-copy">
                            {renderRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
}

export default TableNutrition;
