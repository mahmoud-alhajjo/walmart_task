export const Sort = (nutrition: any, sortBy: string, typeOfSort: boolean, key: string) => {
    let sortNutrition = []
    if (sortBy === "sortByNumber") {
        typeOfSort ? sortNutrition = nutrition?.sort((a: any, b: any) => (a?.nutritionInfo[key] > b.nutritionInfo[key] ? 1 : -1)) :
            sortNutrition = nutrition?.sort((a: any, b: any) => (a?.nutritionInfo[key] > b.nutritionInfo[key] ? -1 : 1))
    } else {
        sortNutrition = nutrition.sort(function (a: any, b: any) {
            var nameA = a[key].toUpperCase(); // ignore upper and lowercase
            var nameB = b[key].toUpperCase(); // ignore upper and lowercase
            if (typeOfSort) {
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
            } else {
                if (nameA < nameB) {
                    return 1;
                }
                if (nameA > nameB) {
                    return -1;
                }
            }
            return 0;
        });
    }
    return sortNutrition;
}
