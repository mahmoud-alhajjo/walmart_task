export default class Validator {

  static isNameValid(name: string, maxLength = 15) {
    if(name.length <= maxLength){
      return true
    } else {
      return false
    }
  }

  static isNumber(number : string) {
    if(number.length <= 5){
      let reg = /^[0-9]*$/;
      return reg.test(number);
    } else {
      return false
    }
  }


  static getNameError(name : string) {
    if ((name == null) || (name === "")) {
      return "Please enter the name of dessert"
    } else {
      if (Validator.isNameValid(name)) {
        return ""
      } else {
        return "Please enter the 15 characters at max"
      }
    }
  }

  static getNumberError(number : string) {
    if ((number == null) || (number === "")) {
      return "please fill out this field with numbers"
    } else {
      if (Validator.isNumber(number)) {
        return "" 
      } else {
        return "Please enter the five-digit number positive at max"
      }
    }
  }

}