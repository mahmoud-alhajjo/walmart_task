import { useState, useEffect } from 'react';
import { useQuery, useMutation } from 'react-query';
import { gql } from 'graphql-request';
import graphqlRequestClient from './lib/clients/graphqlRequestClient';
import { GraphQLResponse } from 'graphql-request/dist/types';
import TableNutrition from './components/TableNutrition';
import Form from './components/Form';
import Modal from './components/modal/Modal';
import { FaRedo } from 'react-icons/fa';
import 'tachyons';

export interface IState {
  nutrition: {
    id: number;
    dessert: string;
    nutritionInfo: {
      calories: number;
      fat: number;
      carb: number;
      protein: number;
    }
  }[]
}

const GET_Nutrition = gql`
  query {
    getNutritions {
      id
      dessert
      nutritionInfo {
        calories
        fat
        carb
        protein
      }
   } 
  }
`;

const RESET_Nutrition = gql`
mutation ResetNutritions {
  resetNutritions {
    id
    dessert
    nutritionInfo {
      calories
      carb
      fat
      protein
    }
  }
}`;

function App() {

  const [nutrition, setNutrition] = useState<IState['nutrition']>([]);
  const [ids, setIds] = useState<number[]>([])
  const [showModal, setShowModal] = useState<boolean>(false)
  const { status, isLoading, error, data } = useQuery<GraphQLResponse, Error, IState['nutrition']>(
    'nutrition',
    async () => {
      return graphqlRequestClient.request(GET_Nutrition)
    },
    {
      select: (response) => response.getNutritions
    }
  )
  useEffect(() => {
    if (status === 'success') {
      setNutrition(data as IState['nutrition']);
    }
  }, [status, data]);
  const { mutate, isLoading: isLoadingReset, error: errorReset } = useMutation<GraphQLResponse, Error, {}>(
    'resetNutritions',
    async (addNewItem) => {
      return graphqlRequestClient.request(RESET_Nutrition)
    },
    {
      onSuccess: (data) => { setNutrition(data.resetNutritions as IState['nutrition']) }
    }
  )

  if (isLoading) return <h2>Loading.....</h2>
  if (error) return <h2>error {error.message}</h2>
  if (isLoadingReset) return <h2>Loading.....</h2>
  if (errorReset) return <h2>error {errorReset.message}</h2>

  return (
    <div className='pv3 bg-light-gray w-100 mw8 center mv2 mh2-m calisto'>
      <header className='flex justify-between items-center w-100 ph4' >
        <h1 className="ma0 f4 fw6 tracked dib f2-ns f3-m">Nutrition List</h1>
        <button className="f7 no-underline dim br1 ph3 pv2 mv2 dib white bg-dark-green ttu bw0 pointer f6-ns" onClick={() => mutate({})} ><FaRedo size={10} /> Reset Data</button>
      </header>
      <TableNutrition
        nutrition={nutrition}
        setNutrition={setNutrition}
        ids={ids}
        setIds={setIds}
        showModal={showModal}
        setShowModal={setShowModal}
      />
      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        children={<Form nutrition={nutrition} setNutrition={setNutrition} showModal={showModal} setShowModal={setShowModal} />}
      />
    </div>
  );
}

export default App;
