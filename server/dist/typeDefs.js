import { gql } from "apollo-server-express";
const typeDefs = gql `
  type NutrationInfo {
    calories: Int
    fat: Int
    carb: Int
    protein: Int
  }
  type Nutrition {
    id: Int
    dessert: String
    nutritionInfo: NutrationInfo
  }
  input NutrationInfoInput {
    calories: Int
    fat: Int
    carb: Int
    protein: Int
  }
  input NutritionData {
    id: Int
    dessert: String
    nutritionInfo: NutrationInfoInput
  }
  type Response {
    success: Boolean
    error: String
  }
  type Query {
    getNutritions: [Nutrition]
  }
  type Mutation {
    createNutrition(nutrition: NutritionData): Response
    deleteNutrition(ids: [Int]): Response
    resetNutritions: [Nutrition]
  }
`;
export default typeDefs;
