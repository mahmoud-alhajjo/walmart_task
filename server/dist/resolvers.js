const initNutritions = [
    {
        id: 1,
        dessert: "Oreo",
        nutritionInfo: {
            calories: 437,
            fat: 18,
            carb: 63,
            protein: 4,
        }
    },
    {
        id: 2,
        dessert: "Nougat",
        nutritionInfo: {
            calories: 360,
            fat: 19,
            carb: 50,
            protein: 37,
        }
    },
];
let nutritions = [
    {
        id: 1,
        dessert: "Oreo",
        nutritionInfo: {
            calories: 437,
            fat: 18,
            carb: 63,
            protein: 4,
        }
    },
    {
        id: 2,
        dessert: "Nougat",
        nutritionInfo: {
            calories: 360,
            fat: 19,
            carb: 50,
            protein: 37,
        }
    },
];
const resolvers = {
    Query: {
        getNutritions: () => nutritions,
    },
    Mutation: {
        createNutrition: (_, newNutrition) => {
            nutritions.push(newNutrition.nutrition);
            const result = {
                success: true,
            };
            return result;
        },
        deleteNutrition: (_, ids) => {
            const remainingItems = nutritions.filter((item) => !ids.ids.includes(item.id));
            nutritions = remainingItems;
            const result = {
                success: true,
            };
            return result;
        },
        resetNutritions: () => {
            nutritions = [
                {
                    id: 1,
                    dessert: "Oreo",
                    nutritionInfo: {
                        calories: 437,
                        fat: 18,
                        carb: 63,
                        protein: 4,
                    }
                },
                {
                    id: 2,
                    dessert: "Nougat",
                    nutritionInfo: {
                        calories: 360,
                        fat: 19,
                        carb: 50,
                        protein: 37,
                    }
                },
            ];
            return nutritions;
        },
    },
};
export default resolvers;
